var express = require('express');
var router = express.Router();

var authentication = require('../utils/authentication');
var Models = require('../models');

// Huom! Kaikki polut alkavat polulla /topics

// GET /topics
router.get('/', function(req, res, next) {
    // Hae kaikki aihealueet tässä (Vinkki: findAll)
    Models.Topic.findAll().then(function(topic) {
    res.json(topic);
    })
});

// GET /topics/:id
router.get('/:id', function(req, res, next) {
  // Hae aihealue tällä id:llä tässä (Vinkki: findOne)
  var topicId = req.params.id;
  console.log(topicId);
  Models.Topic.findOne( {
      where: {
        id: topicId
      },
      include: {
        model: Models.Message,
        include: {
          model: Models.User
        }
      }
  }).then(function(topic) {
    res.json(topic);
  });
});

// POST /topics
router.post('/',authentication, function(req, res, next) {
  // Lisää tämä aihealue
  Models.Topic.create( {
    name: req.body.name,
    description: req.body.description
  }).
    then(function(topic) {
    res.json(topic)
  })
});

// POST /topics/:id/message
router.post('/:id/message', function(req, res, next) {
  // Lisää tällä id:llä varustettuun viestiin...

// Palauta lisätty vastaus
  Models.Message.create({
    title: req.body.title,
    content: req.body.content,
   TopicId: req.params.id,
   UserId: req.session.userId
  }).then(function(message) {
    res.json(message)
  })
});

module.exports = router;
